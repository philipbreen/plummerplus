PlummerPlus
===========

Generates anisotropic and rotation Plummer models, see Breen, Varri & Heggie (2019) for details.


### Features
 - Ansiotropic models of Dejonghe (1987), tangential and radial velocity anisotropy
 - Osipkov�Merritt model  radial velocity anisotropy
 - Rotation introduced via the Lyden-Bell trick (and generalisations)

### Todos

 - include embedded Plummer models using Eddington Formula 
 - Kroupa IMF
